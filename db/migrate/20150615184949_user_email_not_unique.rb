class UserEmailNotUnique < ActiveRecord::Migration
  def change
    remove_index :contacts, :email
  end
end
