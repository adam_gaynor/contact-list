class MakeContactIdNotUnique < ActiveRecord::Migration
  def change
    remove_index :contact_shares, :contact_id
    add_index :contact_shares, :contact_id
  end
end
