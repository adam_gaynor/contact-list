class ContactShareNotUniqueToUser < ActiveRecord::Migration
  def change
    remove_index :contact_shares, :user_id
    add_index :contact_shares, :user_id
  end
end
