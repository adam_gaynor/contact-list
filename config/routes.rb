FirstRoutes::Application.routes.draw do
  resources :users, only: [:create, :index, :update, :show, :destroy] do
    resources :contacts, only: :index
  end
  resources :contacts, only: [:create, :update, :show, :destroy]
  resources :contact_shares, only: [:destroy, :create]
end
